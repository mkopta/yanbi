#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MEMSIZE 30000

static void indent(int indentlevel)
{
	int i;
	for (i = 0; i < indentlevel; i++)
		printf("\t");
}

static int translate(FILE *src) {
	int c, indentlevel = 1;
	while ((c = getc(src)) != EOF) {
		switch (c) {
		case '>':
			indent(indentlevel);
			printf("++d;\n");
			break;
		case '<':
			indent(indentlevel);
			printf("--d;\n");
			break;
		case '+':
			indent(indentlevel);
			printf("++*d;\n");
			break;
		case '-':
			indent(indentlevel);
			printf("--*d;\n");
			break;
		case '.':
			indent(indentlevel);
			printf("(void) putchar(*d);\n");
			break;
		case ',':
			indent(indentlevel);
			printf("*d = getchar();\n");
			break;
		case '[':
			indent(indentlevel);
			indentlevel++;
			printf("while (*d) {\n");
			break;
		case ']':
			indentlevel--;
			indent(indentlevel);
			printf("}\n");
			break;
		default:
			break;
		}
	}
	return 0;
}

void print_intro(void) {
	puts("#include <string.h>");
	puts("#include <stdlib.h>");
	puts("#include <stdio.h>\n");
	puts("#define MEMSIZE 30000\n");
	puts("int main(void) {");
	puts("\tchar *d = NULL, *mem = malloc(MEMSIZE * sizeof(char));");
	puts("\tif (mem == NULL) {");
	puts("\t\tperror(\"malloc\");");
	puts("\t\treturn 1;");
	puts("\t}");
	puts("\tmemset(mem, 0, MEMSIZE * sizeof(char));");
	puts("\td = mem;");
}

void print_outro(void) {
	puts("\tfree(mem);");
	puts("\treturn 0;");
	puts("}");
}

int main(const int argc, const char *const *argv) {
	FILE *src = NULL;
	int retval = 0;
	if (argc != 2) {
		fputs("Usage: yanbi <source.bf>\n", stderr);
		return 1;
	}
	if ((src = fopen(*++argv, "r")) == NULL) {
		perror("open");
		return 1;
	}
	print_intro();
	retval = translate(src);
	print_outro();
	fclose(src);
	return retval;
}
