CC        = cc
CFLAGS    = -ansi
CFLAGS   += -Wall -Wextra -Wconversion -pedantic
CFLAGS   += -DDEBUG -ggdb3

all: build

build: yanbi

yanbi: yanbi.c
	$(CC) $(CFLAGS) -o yanbi yanbi.c

clean:
	rm -f yanbi
